**Class for PDO**

_Example:_

```
require_once(__DIR__ . '/classes/Config.php');
require_once(__DIR__ . '/classes/Log.php');
require_once(__DIR__ . '/classes/Database.php');
require_once(__DIR__ . '/classes/Input.php');
$db = Database::getInstance();
```
