<?php

class Database{

    public $isConn;
    protected $datab;
	private static $instance = null;
    private $log;
	
	private $host = '';
	private $username = '';
	private $password = '';
	private $dbname = '';
	private $charset = 'utf8mb4';

    // connect to db
    public function __construct(){
        $this->host     = Config::read('db.host');
        $this->username = Config::read('db.user');
        $this->password = Config::read('db.password');
        $this->dbname   = Config::read('db.basename');

        $this->isConn = TRUE;
        $dsn = 'mysql:dbname=' . $this->dbname . ';host=' . $this->host . ';charset='.$this->charset;
        $options = array(
    		PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    		PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    		PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES '.$this->charset
		);

        try {
            $this->datab = new PDO($dsn, $this->username, $this->password, $options);

            # Disable emulation of prepared statements, use REAL prepared statements instead.
            $this->datab->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }

        $this->log = new Log();
    }
	
	private function __clone () {}
	private function __wakeup () {}

	public static function getInstance()
	{
		if(!self::$instance)
		{
			self::$instance = new Database();
		}

		return self::$instance;
	}

	public function getConnection()
	{
		return $this->datab;
	}


    // disconnect from db
    public function Disconnect(){
        $this->datab = NULL;
        $this->isConn = FALSE;
    }


    // get row
    public function getRow($query, $params = []){
		$instance = Database::getInstance();
		$conn = $instance->getConnection();
        try {
            $stmt = $conn->prepare($query);
            $stmt->execute($params);
            return $stmt->fetch();
        } catch (PDOException $e) {
            $message = $e->getMessage();
            $error = $e->getTrace();
            
            # Пишем в лог
            $this->log->writelog('sql_error', $error, $message);
        }
    }

    // get rows
    public function getRows($query, $params = []){
		$instance = Database::getInstance();
		$conn = $instance->getConnection();
        try {
            $stmt = $conn->prepare($query);
            $stmt->execute($params);
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            $message = $e->getMessage();
            $error = $e->getTrace();
            
            # Пишем в лог
            $this->log->writelog('sql_error', $error, $message);
        }
    }

    // insert row
    public function insertRow($query, $params = []){
		$instance = Database::getInstance();
		$conn = $instance->getConnection();
        try {
            $stmt = $conn->prepare($query);
            $stmt->execute($params);
            return TRUE;
        } catch (PDOException $e) {
            $message = $e->getMessage();
            $error = $e->getTrace();
            
            # Пишем в лог
            $this->log->writelog('sql_error', $error, $message);
        }
    }

    // update row
    public function updateRow($query, $params = []){
        try {
            $this->insertRow($query, $params);
            return TRUE;
        } catch (PDOException $e) {
            $message = $e->getMessage();
            $error = $e->getTrace();
            
            # Пишем в лог
            $this->log->writelog('sql_error', $error, $message);
        }
    }

    // delete row
    public function deleteRow($query, $params = []){
        return $this->insertRow($query, $params);
    }

    public function getColumn($query, $params = []){
		$instance = Database::getInstance();
		$conn = $instance->getConnection();
        try {
            $stmt = $conn->prepare($query);
            $stmt->execute($params);
            return $stmt->fetchColumn();
        } catch (PDOException $e) {
            $message = $e->getMessage();
            $error = $e->getTrace();
            
            # Пишем в лог
            $this->log->writelog('sql_error', $error, $message);
        }
    }

    public function rowCount($query, $params = []){
		$instance = Database::getInstance();
		$conn = $instance->getConnection();
        try {
            $stmt = $conn->prepare($query);
            $stmt->execute($params);
            return $stmt->rowCount();
        } catch (PDOException $e) {
            $message = $e->getMessage();
            $error = $e->getTrace();
            
            # Пишем в лог
            $this->log->writelog('sql_error', $error, $message);
        }
    }

    public function FetchRow($query, $params = []){
		$instance = Database::getInstance();
		$conn = $instance->getConnection();
        try {
            $stmt = $conn->prepare($query);
            $stmt->execute($params);
            return $stmt->FetchRow();
        } catch (PDOException $e) {
            $message = $e->getMessage();
            $error = $e->getTrace();
            
            # Пишем в лог
            $this->log->writelog('sql_error', $error, $message);
        }
    }

    public function getlastInsertId(){
		$instance = Database::getInstance();
		$conn = $instance->getConnection();
        try {
            return $conn->lastInsertId();
        } catch (PDOException $e) {
            $message = $e->getMessage();
            $error = $e->getTrace();
            
            # Пишем в лог
            $this->log->writelog('sql_error', $error, $message);
        }
    }
	
	public function getExecute($query, $params = []){
		$instance = Database::getInstance();
		$conn = $instance->getConnection();
        try {
            $stmt = $conn->prepare($query);
            $stmt->execute($params);
            return TRUE;
        } catch (PDOException $e) {
            $message = $e->getMessage();
            $error = $e->getTrace();
            
            # Пишем в лог
            $this->log->writelog('sql_error', $error, $message);
        }
	}

}

?>